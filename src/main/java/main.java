import br.pucminas.pedidos.dominio.Pedido;
import br.pucminas.pedidos.dominio.Produto;


public class main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Produto p = new Produto(1, "lapis", 0.30);
		Produto p2 = new Produto(2, "papel", 0.90);
		Pedido pedido = new Pedido(1);
		pedido.incluiItem(p, 5);
		pedido.incluiItem(p2, 10);
		
		pedido.atualizaQuantidade(p, -30);
		pedido.atualizaQuantidade(p2, 15);
		pedido.imprimeArray();
	}

}
