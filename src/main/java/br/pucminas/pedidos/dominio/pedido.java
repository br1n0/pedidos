package br.pucminas.pedidos.dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import org.junit.Test;

//import javax.swing.text.html.HTMLDocument.Iterator;

public class Pedido {
	private int numero;
	private Calendar dataDaRealizacao;
	private Collection<ItemDoPedido> itensDoPedido;

	public Pedido(int numero) {
		if (numero < 0){
			throw new IllegalArgumentException("O número do pedido deverá ser maior ou igual a zero!");
		}
		this.numero = numero;
		this.dataDaRealizacao = Calendar.getInstance();
		this.itensDoPedido = new ArrayList<ItemDoPedido>();
	}

	public void incluiItem(Produto produto, double quantidade) {
		this.itensDoPedido.add(new ItemDoPedido(produto, quantidade));
	}

	public double calculaTotal(){
		double total = 0.0;
		for (ItemDoPedido itemDoPedido: itensDoPedido){
			total += itemDoPedido.calculaTotal();
		}
		return total;
	}
	@Override
	public String toString() {
		return String.format("Pedido %03d# data=%2$te/%2$tm/%2$tY", this.numero,
				this.dataDaRealizacao);
	}
	
	public void atualizaQuantidade(Produto p, double n){
	
		for (java.util.Iterator<ItemDoPedido> iterator = itensDoPedido.iterator(); iterator.hasNext();) {
	        ItemDoPedido teste = (ItemDoPedido) iterator.next();
	        Produto prod = teste.getProduto();
	        if(prod.equals(p)){
	        	teste.setQuantidade(teste.getQuantidade()+n);
	        }
	        if(teste.getQuantidade()<1){
	        	itensDoPedido.remove(teste);
	        	throw new IllegalArgumentException("Năo é possivel atualizar a quantidade de um pedido inexistente");
	        }
	        	
	    }
	}
	
	public void transfereQuantidade(Produto p, Produto p2, double n){
	
		for (java.util.Iterator<ItemDoPedido> iterator = itensDoPedido.iterator(); iterator.hasNext();) {
	        ItemDoPedido teste = (ItemDoPedido) iterator.next();
	        Produto prod = teste.getProduto();
	        if(prod.equals(p)){
	        	teste.setQuantidade(teste.getQuantidade()+n);
	        }
	        else if(prod.equals(p2)){
	        	teste.setQuantidade(teste.getQuantidade()-n);
	        }
	        if(teste.getQuantidade()<1){
	        	itensDoPedido.remove(teste);
	        	throw new IllegalArgumentException("Nao e possivel transferir a quantidade de um item inexistente");
	        }
	        	
	    }
	}
	
	public void imprimeArray(){
		for (java.util.Iterator<ItemDoPedido> iterator = itensDoPedido.iterator(); iterator.hasNext();) {
		ItemDoPedido imprimeItem = (ItemDoPedido) iterator.next();
		imprimeItem.imprime();
		}
	}
}
