package br.pucminas.pedidos.dominio;

public class ItemDoPedido {
	private Produto produto;
	private double quantidade;

	public ItemDoPedido(Produto produto, double quantidade) {
		this.produto = produto;
		this.quantidade = quantidade;
	}

	public double calculaTotal() {
		return this.produto.obtemPreco() * this.quantidade;
	}

	public Produto getProduto(){
		return this.produto;
	}
	
	public double getQuantidade(){
		return this.quantidade;
	}
	
	public void setQuantidade(double q){
		quantidade=q;
	}
	
	@Override
	public String toString() {
		return String.format("ItemDoPedido : produto=%s, quantidade=%.2f",
				this.produto, this.quantidade);
	}
	
	public void imprime(){
		System.out.println("Quantidade:"+quantidade);
	}
}
