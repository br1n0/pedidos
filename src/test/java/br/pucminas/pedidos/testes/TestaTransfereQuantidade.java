package br.pucminas.pedidos.testes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucminas.pedidos.dominio.Pedido;
import br.pucminas.pedidos.dominio.Produto;
	//Author:Raissa
public class TestaTransfereQuantidade {
	private static Produto p;
	private static Produto p2;
	private static Produto p3;
	private Pedido pedido;
	
	@BeforeClass
	public static void configuraClasseDeTeste() {
		p = new Produto(1, "lapis", 0.30);
		p2 = new Produto(2, "papel", 0.90);
		p3 = new Produto(3, "borracha", 1.50);
	}
	@Before 
	public void configura(){
		pedido = new Pedido(1);
		pedido.incluiItem(p, 10);
		pedido.incluiItem(p2, 20);
		pedido.incluiItem(p3, 30);
		
	}
	
	@Test
	public void aQuantidadeDeveraSerAtualizadaComSucesso(){
		pedido.transfereQuantidade(p, p2, 10);
		pedido.imprimeArray();
	}
	
	
	@Test
	public void aQuantidadeNegativaDeveraRemoverEsteItem(){
		pedido.transfereQuantidade(p, p2, -30);
		pedido.imprimeArray();
	}
	
	@Test
	public void aQuantidadeASerTransferidaNaoPodeExcederAExistente(){
		pedido.transfereQuantidade(p, p3, 40);
		pedido.imprimeArray();
	}
	
	
	@After
	public void desconfigura(){
		pedido = null;
	}
	
	@AfterClass
	public static void desconfiguraClasseDeTeste() {
		p = null;
		p2 = null;
		p3 = null;
	}
}


