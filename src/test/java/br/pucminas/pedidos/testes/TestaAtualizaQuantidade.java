package br.pucminas.pedidos.testes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucminas.pedidos.dominio.Pedido;
import br.pucminas.pedidos.dominio.Produto;
	
public class TestaAtualizaQuantidade {
	private static Produto p;
	private static Produto p2;
	private Pedido pedido;
	
	@BeforeClass
	public static void configuraClasseDeTeste() {
		p = new Produto(1, "lapis", 0.30);
		p2 = new Produto(2, "papel", 0.90);
	}
	@Before 
	public void configura(){
		pedido = new Pedido(1);
		pedido.incluiItem(p, 5);
		pedido.incluiItem(p2, 10);
	}
	
	@Test
	public void aQuantidadeDeveraSerAtualizadaComSucesso(){
		pedido.atualizaQuantidade(p, 10);
		pedido.imprimeArray();
	}
	
	@Test
	public void aQuantidadeNegativaDeveraRemoverEsteItem(){
		pedido.atualizaQuantidade(p, -30);
		pedido.imprimeArray();
	}
	
	@Test
	public void aQuantidadeDeUmItemInexistenteDeveraRetornarUmaExcecao(){
		pedido.atualizaQuantidade(p, 10);
		pedido.imprimeArray();
	}
	
	@After
	public void desconfigura(){
		pedido = null;
	}
	
	@AfterClass
	public static void desconfiguraClasseDeTeste() {
		p = null;
		p2 = null;
	}
}


