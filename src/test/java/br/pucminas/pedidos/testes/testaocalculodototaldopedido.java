package br.pucminas.pedidos.testes;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucminas.pedidos.dominio.Pedido;
import br.pucminas.pedidos.dominio.Produto;

public class TestaOCalculoDoTotalDoPedido {
	
	private static Produto livro;
	private static Produto lapis;
	private Pedido pedido;

	@BeforeClass
	public static void configuraClasseDeTeste() {
		livro = new Produto(1, "UML e Padr�es", 136.00, 0);
		lapis = new Produto(2, "L�pis Preto", 1.50, 0);
	}
	@Before 
	public void configura(){
		pedido = new Pedido(1);
	}
	@Test
	public void oTotalDoPedidoDeveraRetornarZeroParaUmPedidoSemItens() {
		assertEquals(0.0, pedido.calculaTotal(), 0.01);
	}

	@Test
	public void oTotalDoPedidoDeveraRetornarUmValorParaUmPedidoComUmItem() {
		pedido.incluiItem(livro, 1);
		assertEquals(136.00, pedido.calculaTotal(), 0.01);
	}

	@Test
	public void oTotalDoPedidoDeveraRetornarUmValorParaUmPedidoComDoisItens() {
		pedido.incluiItem(livro, 1);
		pedido.incluiItem(lapis, 1);
		assertEquals(137.50, pedido.calculaTotal(), 0.01);
	}

	
	@After
	public void desconfigura(){
		pedido = null;
	}
	
	@AfterClass
	public static void desconfiguraClasseDeTeste() {
		livro = null;
		lapis = null;
	}
}
