package br.pucminas.pedidos.testes;

import org.junit.Test;

import br.pucminas.pedidos.dominio.Pedido;

public class TestaACriacaoDoPedido {

	@Test(expected=IllegalArgumentException.class)
	public void aCriacaoDoPedidoDeveraRetornarUmExcecaoParaUmNumeroDePedidoNegativo() {
		Pedido pedido = new Pedido(-1);
	}

}
